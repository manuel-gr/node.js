import http from 'http';
import path from 'path';
import express from 'express';
import bodyParser from 'body-parser';
import { fileURLToPath } from 'url';
import misRutas from './router/index.js';

const puerto = 80;

const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);

const port = 80;
const app = express();

// Asignaciones
app.set("view engine", "ejs");
app.use(express.urlencoded({ extended: true }));
app.use(express.static(__dirname + '/public'));

app.use(misRutas.router);

app.listen(port, () => {
    console.log("Iniciando el servidor en el puerto:", port);
});

app.set('view engine', 'ejs');
