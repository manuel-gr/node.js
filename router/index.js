import express from 'express';
export const router = express.Router();
import conexion from '../models/conexion.js';
import alumnosDb from "../models/alumnos.js";

router.get('/', (req, res) => {
    res.render('index', { titulo: "Mi Primer Pagina ejs", nombre: "José Manuel González Ramírez" });
});
router.get('/practicas', (req, res) => {
    res.render('practicas/index', { titulo: "Mi Primer Pagina ejs", nombre: "José Manuel González Ramírez" });
});

router.get('/ExamenC2', (req, res) => {
    res.render('ExamenC2/index', { titulo: "Mi Primer Pagina ejs", nombre: "José Manuel González Ramírez" });
});

router.get('/practicas/tabla', (req, res) => {
    const params = {
        numero: req.query.numero,
    }
    res.render('practicas/tabla', params);
});

router.post('/practicas/tabla', (req, res) => {
    const params = {
        numero: req.body.numero,
    }
    res.render('practicas/tabla', params);
});


router.get('/practicas/cotizacion', (req, res) => {
    res.render('practicas/cotizacion');
});

router.post('/practicas/cotizacion', (req, res) => {
    const { precio, porcentajeInicial, plazo } = req.body;

    // Validar que el porcentaje de pago inicial no sea negativo ni supere el 100%
    if (parseFloat(porcentajeInicial) < 0) {
        const error = 'El porcentaje de pago inicial no puede ser negativo.';
        return res.render('practicas/cotizacion', { error });
    }

    if (parseFloat(porcentajeInicial) > 100) {
        const error = 'El porcentaje de pago inicial no puede superar el 100%.';
        return res.render('practicas/cotizacion', { error });
    }

    // Validar que el precio y plazo no sean negativos
    if (parseFloat(precio) < 0 || parseInt(plazo) < 0) {
        const error = 'El precio y el plazo no pueden ser negativos.';
        return res.render('practicas/cotizacion', { error });
    }

    // Resto del cálculo
    const pagoInicial = (parseFloat(porcentajeInicial) / 100) * parseFloat(precio);
    const totalFinanciar = parseFloat(precio) - pagoInicial;
    const pagoMensual = totalFinanciar / parseInt(plazo);

    res.render('practicas/cotizacion', { 
        porcentajeInicial,
        precio,
        plazo,
        pagoInicial,
        totalFinanciar,
        pagoMensual
    });
});

let rows;
router.get('/practicas/alumnos',async(req,res)=>{
    rows = await alumnosDb.mostrarTodos();

    res.render('practicas/alumnos',{reg:rows});
})

let params;
router.post('/practicas/alumnos', async(req,res)=>{
    try {

    params ={
        matricula:req.body.matricula,
        nombre:req.body.nombre,
        domicilio:req.body.domicilio,
        sexo : req.body.sexo,
        especialidad:req.body.especialidad
    }
    const registros = await alumnosDb.insertar(params);
    console.log("-------------- registros " + registros);

    } catch(error){
        console.error(error)
        res.status(400).send("sucedio un error: " + error);
    }
    rows = await alumnosDb.mostrarTodos();
    res.render('practicas/alumnos',{reg:rows});
});
  

async function prueba(){
    try {
        const res = await conexion.execute("select * from alumnos");
        console.log("El resultado es ", res);
    } catch (error) { 
        console.log("Surgió un error", error);
    } finally {
        conexion.end();
    }
}
    
router.post("/practicas/buscar", async(req,res)=>{
    matricula = req.body.matricula;
    row = await alumnosDb.consultarMatricula(matricula);
    res.render("practicas/alumnos", {alu:nrow});


})

router.get('/preexamen1', (req, res) => {
    res.render('preexamen1/index');
});

router.get('/preexamen1/pago', (req, res) => {
    // Parámetros
    const params = {
        numeroRecibo: req.query.numeroRecibo,
        nombre: req.query.nombre,
        domicilio: req.query.domicilio,
        tipoServicio: req.query.tipoServicio,
        kw: req.query.kw,
    }
    res.render('preexamen1/pago', params);
});

router.post('/preexamen1/pago', (req, res) => {
    // Parámetros
    const params = {
        numeroRecibo: req.body.numeroRecibo,
        nombre: req.body.nombre,
        domicilio: req.body.domicilio,
        tipoServicio: req.body.tipoServicio,
        kw: req.body.kw,
    }
    res.render('preexamen1/pago', params);
});

router.get('/ExamenC2/detalles', (req, res) => {
     // Parámetros
    const params = {
        num_docente: req.query.num_docente,
        nombre: req.query.nombre,
        domicilio: req.query.domicilio,
        nivel: req.query.nivel,
        pago_base: req.query.pago_base,
        horas_impartidas: req.query.horas_impartidas,
        cantidad_hijos: req.query.cantidad_hijos,
    }
   res.render('ExamenC2/detalles', params);
});

router.post('/ExamenC2/detalles', (req, res) => {
    // Parámetros obtenidos del formulario
    const params = {
        num_docente: parseInt(req.body.num_docente),
        nombre: req.body.nombre,
        domicilio: req.body.domicilio,
        nivel: parseInt(req.body.nivel),
        pago_base: parseFloat(req.body.pago_base),
        horas_impartidas: parseInt(req.body.horas_impartidas),
        cantidad_hijos: parseInt(req.body.cantidad_hijos),
    };
   
    // Renderizar la vista 'detalles.ejs' con los parámetros
    res.render('ExamenC2/detalles', params);
});


export default {router};
