import { promises } from "stream";
import conexion from "./conexion.js";
import { rejects } from "assert";
import { error } from "console";
var alumnosDb = {}

alumnosDb.insertar = function insertar(alumno){
    return new Promise((resolve,rejects)=>{
        // consulta
        let sqlConsulta = "Insert into alumnos set ?";
        conexion.query(sqlConsulta,alumno,function(err,res){
            if(err){
                console.log("Surgio un error ",err.message);
                rejects(err);
            }
            else {
            const alumno = {
                id:res.id,
            }
            resolve(alumno);
            }
        });
    });
}

alumnosDb.mostrarTodos = function mostrarTodos() {
    return new Promise((resolve, reject)=> {
        var sqlConsulta = "select * from alumnos";
        conexion.query(sqlConsulta, null, function(err, res) {
            if(err) {
                console.log("Surgio un error");
                reject(err);
            }
            else {
                resolve(res);
            }
        });
    });
}

alumnosDb.consultarMatricula = function consultarMatricula(matricula){
    return new Promise((resolve, reject)=> {
        var sqlConsulta = "select * from alumnos where matricula";
        conexion.query(sqlConsulta, [matricula], function(err, res) {
            if(err) {
                console.log("Surgio un error", err.message);
                reject(err);
            }
            else {
                resolve(res);
            }
        });
    });
}

alumnosDb.borrarMatricula = function borrarMatriculaMatricula(matricula){
    return new Promise((resolve, reject)=> {
        var sqlConsulta = "delete from alumnos where matricula";
        conexion.query(sqlConsulta, [matricula], function(err, res) {
            if(err) {
                console.log("Surgio un error", err.message);
                reject(err);
            }
            else {
                resolve(res);
            }
        });
    });
}
    
alumnosDb.actualizarMatricula = function actualizarMatricula(matricula){
    return new Promise((resolve, reject)=> {
        const consulta = "UPDATE alumnos SET nombre = ?,domicilio = ?,sexo = ?,especialidad = ? WHERE matricula";
        conexion.query(consulta, [nombre,domicilio,sexo,especialidad,matricula], (err, res) => {
            if(err) {
                console.log("Surgio un error", err);
                reject(err);
            }
            else {
                resolve(res);
            }
        });
    });
}
export default alumnosDb;